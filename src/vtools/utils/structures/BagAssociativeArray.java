package vtools.utils.structures;

/**
 * An associative Array that allows duplicates on its keys
 */
public class BagAssociativeArray extends AssociativeArray
{

    public BagAssociativeArray()
    {
        super();
        super._duplicatesAllowed = true;
    }

    


}
